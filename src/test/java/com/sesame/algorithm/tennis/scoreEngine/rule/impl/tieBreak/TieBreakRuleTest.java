package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.test.Util;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TieBreakRuleTest {

    TieBreakRule tieBreakRule = new TieBreakRule();
    Player p0,p1;
    List<Player> players;

    @Test
    public void testNoChangeForRegularMatchByTieBreakRule() throws Exception {
        p0 = new Player("A", 5, 1, 1, null);
        p1 = new Player("B", 6, 0, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());

        tieBreakRule.apply(players);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());

    }

    @Test
    public void testChangeForTieBreakMatchByTieBreakRule() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 1, 6, 1, null);
        p1 = new Player("B", 2, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());


        tieBreakRule.apply(players);

        Assert.assertEquals("1", p0.getGamePointVerbal());
        Assert.assertEquals("2", p1.getGamePointVerbal());
        Assert.assertEquals("1-2", p0.getSummeryPointVerbal());
        Assert.assertEquals("1-2", p1.getSummeryPointVerbal());
    }


    @Test
    public void testNotYetWinningTieBreakMatchByTieBreakRule() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 7, 6, 1, null);
        p1 = new Player("B", 6, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(7, p0.getGamePoint());
        Assert.assertEquals(6, p1.getGamePoint());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());


        tieBreakRule.apply(players);

        //reset to zero after game is won, for next game
        Assert.assertEquals("7", p0.getGamePointVerbal());
        Assert.assertEquals("6", p1.getGamePointVerbal());

        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

        //below empty value or reseted to zero after game is won, for next game
        Assert.assertEquals("7-6", p0.getSummeryPointVerbal());
        Assert.assertEquals("7-6", p1.getSummeryPointVerbal());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(7, p0.getGamePoint());
        Assert.assertEquals(6, p1.getGamePoint());


    }

    @Test
    public void testWinningTieBreakMatchByTieBreakRule() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 8, 6, 1, null);
        p1 = new Player("B", 6, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(8, p0.getGamePoint());
        Assert.assertEquals(6, p1.getGamePoint());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());
        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());


        tieBreakRule.apply(players);


        Assert.assertEquals(2, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

        //below empty value or reseted to zero after game is won, for next game
        Assert.assertEquals(0, p0.getGameScore());
        Assert.assertEquals(0, p1.getGameScore());
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals("0", p0.getGamePointVerbal());
        Assert.assertEquals("0", p1.getGamePointVerbal());


    }
}