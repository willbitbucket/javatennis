package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PointVerbalActionTest {


    Player p0,p1;
    List<Player> players;
    Action action = new PointVerbalAction();

    @Test
    public void testExecute() throws Exception {
        p0 = new Player("A", 1, 1, 1, null);
        p1 = new Player("B", 3, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        action.execute(players);

        Assert.assertEquals("1", p0.getGamePointVerbal());
        Assert.assertEquals("3", p1.getGamePointVerbal());
    }
}