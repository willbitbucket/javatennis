package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.test.Util;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GameActionTest {
    Player p0,p1;
    List<Player> players;
    GameAction gameAction = new GameAction();

    @Test
    public void testWonGameAsWonFirst6GamesAnd2GamesMoreThanOpponent() throws Exception {
        p0 = new Player("A", 4, 1, 1, null);
        p1 = new Player("B", 6, 1, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals(1, p0.getGameScore());
        Assert.assertEquals(2, p1.getGameScore());
    }

    @Test
    public void testNotWinGameAsNotWon2GamesMore() throws Exception {
        p0 = new Player("A", 5, 1, 1, null);
        p1 = new Player("B", 6, 1, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(5, p0.getGamePoint());
        Assert.assertEquals(6, p1.getGamePoint());
        Assert.assertEquals(1, p0.getGameScore());
        Assert.assertEquals(1, p1.getGameScore());
    }

    @Test
    public void testWonGame2AsNotWonAtLeast6Game() throws Exception {
        p0 = new Player("A", 1, 1, 1, null);
        p1 = new Player("B", 3, 1, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(1, p0.getGamePoint());
        Assert.assertEquals(3, p1.getGamePoint());
        Assert.assertEquals(1, p0.getGameScore());
        Assert.assertEquals(1, p1.getGameScore());
    }
}