package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SetActionTest {

    Player p0,p1;
    List<Player> players;
    Action setAction = new SetAction();

    @Test
    public void testWonSet() throws Exception {
        p0 = new Player("A", 4, 6, 1, null);
        p1 = new Player("B", 6, 7, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        setAction.execute(players);
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals(0, p0.getGameScore());
        Assert.assertEquals(0, p1.getGameScore());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(3, p1.getSetScore());
    }

    @Test
    public void testNotWonSetAsNotWonMoreThan7Games() throws Exception {
        p0 = new Player("A", 6, 6, 1, null);
        p1 = new Player("B", 6, 6, 3, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        setAction.execute(players);
        Assert.assertEquals(6, p0.getGamePoint());
        Assert.assertEquals(6, p1.getGamePoint());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(3, p1.getSetScore());
    }
}