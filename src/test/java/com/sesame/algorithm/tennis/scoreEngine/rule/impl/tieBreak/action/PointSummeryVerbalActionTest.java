package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PointSummeryVerbalActionTest {


    Player p0,p1;
    List<Player> players;
    Action action = new PointVerbalSummeryAction();

    @Test
    public void testExecute() throws Exception {
        p0 = new Player("A", 1, 1, 1, null);
        p1 = new Player("B", 3, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        //mock
        p0.setGamePointVerbal("1");
        p1.setGamePointVerbal("3");

        action.execute(players);

        Assert.assertEquals("1-3", p0.getSummeryPointVerbal());
        Assert.assertEquals("1-3", p1.getSummeryPointVerbal());
    }

    @Test
    public void testDence() throws Exception {
        p0 = new Player("A", 4, 1, 1, null);
        p1 = new Player("B", 4, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        //mock
        p0.setGamePointVerbal("4");
        p1.setGamePointVerbal("4");


        action.execute(players);

        Assert.assertEquals("4-4", p0.getSummeryPointVerbal());
        Assert.assertEquals("4-4", p1.getSummeryPointVerbal());
    }

    @Test
    public void testAdvantage() throws Exception {
        p0 = new Player("A", 5, 1, 1, null);
        p1 = new Player("B", 4, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);
        //mock
        p0.setGamePointVerbal("5");
        p1.setGamePointVerbal("4");

        action.execute(players);

        Assert.assertEquals("5-4", p0.getSummeryPointVerbal());
        Assert.assertEquals("5-4", p1.getSummeryPointVerbal());
    }
}