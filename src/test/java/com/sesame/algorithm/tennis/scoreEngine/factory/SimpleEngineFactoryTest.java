package com.sesame.algorithm.tennis.scoreEngine.factory;

import com.sesame.algorithm.tennis.scoreEngine.Engine;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.*;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.TieBreakRule;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class SimpleEngineFactoryTest {

    SimpleEngineFactory simpleEngineFactory;

    @Test
    public void testNewEngine() throws Exception {
        ArrayList<Rule> rules = new ArrayList();
        Rule r0 = new RegularRule();
        Rule r1 = new TieBreakRule();

        rules.add(r0);
        rules.add(r1);

        Engine engine =  SimpleEngineFactory.newEngine(rules);
        Assert.assertEquals(2, engine.getRules().size());
        Assert.assertEquals(r1, engine.getRules().get(1));
    }
}