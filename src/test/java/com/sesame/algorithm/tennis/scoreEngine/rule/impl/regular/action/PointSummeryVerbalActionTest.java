package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PointSummeryVerbalActionTest {


    Player p0,p1;
    List<Player> players;
    Action action = new PointVerbalSummeryAction();

    @Test
    public void testExecute() throws Exception {
        p0 = new Player("A", 1, 1, 1, null);
        p1 = new Player("B", 3, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        //mock
        p0.setGamePointVerbal("15");
        p1.setGamePointVerbal("40");

        action.execute(players);

        Assert.assertEquals("15-40", p0.getSummeryPointVerbal());
        Assert.assertEquals("15-40", p1.getSummeryPointVerbal());
    }

    @Test
    public void testDence() throws Exception {
        p0 = new Player("A", 4, 1, 1, null);
        p1 = new Player("B", 4, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        action.execute(players);

        Assert.assertEquals("Dence", p0.getSummeryPointVerbal());
        Assert.assertEquals("Dence", p1.getSummeryPointVerbal());
    }

    @Test
    public void testAdvantage() throws Exception {
        p0 = new Player("A", 5, 1, 1, null);
        p1 = new Player("B", 4, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        action.execute(players);

        Assert.assertEquals("Advantage A", p0.getSummeryPointVerbal());
        Assert.assertEquals("Advantage A", p1.getSummeryPointVerbal());
    }
}