package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SetActionTest {

    Player p0,p1;
    List<Player> players;
    Action setAction = new SetAction();

    @Test
    public void testWonSet() throws Exception {
        p0 = new Player("A", 4, 1, 1, null);
        p1 = new Player("B", 6, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        setAction.execute(players);
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals(0, p0.getGameScore());
        Assert.assertEquals(0, p1.getGameScore());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(3, p1.getSetScore());
    }

    @Test
    public void testNotWinSetAsNotWonMoreThan6Games() throws Exception {
        p0 = new Player("A", 1, 1, 1, null);
        p1 = new Player("B", 5, 5, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        setAction.execute(players);
        Assert.assertEquals(1, p0.getGamePoint());
        Assert.assertEquals(5, p1.getGamePoint());
        Assert.assertEquals(1, p0.getGameScore());
        Assert.assertEquals(5, p1.getGameScore());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

    }
}