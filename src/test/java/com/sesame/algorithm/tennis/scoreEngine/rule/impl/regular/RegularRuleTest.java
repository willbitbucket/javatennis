package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular;

import com.sesame.algorithm.tennis.pojo.Player;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RegularRuleTest {

    RegularRule regularRule = new RegularRule();
    Player p0,p1;
    List<Player> players;

    @Test
    public void testNoChangeForTieBreakMatchByRegularRule() throws Exception {
        p0 = new Player("A", 5, 6, 1, null);
        p1 = new Player("B", 6, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());

        regularRule.apply(players);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());

    }

    @Test
    public void testChangeForRegularMatchByRegularRule() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 1, 5, 1, null);
        p1 = new Player("B", 2, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());


        regularRule.apply(players);

        Assert.assertEquals("15", p0.getGamePointVerbal());
        Assert.assertEquals("30", p1.getGamePointVerbal());
        Assert.assertEquals("15-30", p0.getSummeryPointVerbal());
        Assert.assertEquals("15-30", p1.getSummeryPointVerbal());
    }


    @Test
    public void testDence() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 7, 5, 1, null);
        p1 = new Player("B", 7, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals(5, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(7, p0.getGamePoint());
        Assert.assertEquals(7, p1.getGamePoint());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());


        regularRule.apply(players);

        //reset to zero after game is won, for next game

        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

        //below empty value or reseted to zero after game is won, for next game
        Assert.assertEquals("Dence", p0.getSummeryPointVerbal());
        Assert.assertEquals("Dence", p1.getSummeryPointVerbal());
        Assert.assertEquals(5, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(7, p0.getGamePoint());
        Assert.assertEquals(7, p1.getGamePoint());


    }

    @Test
    public void testAdvantage() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 8, 5, 1, null);
        p1 = new Player("B", 7, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals(5, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(8, p0.getGamePoint());
        Assert.assertEquals(7, p1.getGamePoint());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());


        regularRule.apply(players);

        //reset to zero after game is won, for next game

        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

        //below empty value or reseted to zero after game is won, for next game
        Assert.assertEquals("Advantage A", p0.getSummeryPointVerbal());
        Assert.assertEquals("Advantage A", p1.getSummeryPointVerbal());
        Assert.assertEquals(5, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(8, p0.getGamePoint());
        Assert.assertEquals(7, p1.getGamePoint());

    }

    @Test
    public void testWinning() throws Exception {
        //assuming this is the status after the game point change
        p0 = new Player("A", 7, 5, 1, null);
        p1 = new Player("B", 9, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        Assert.assertEquals("", p0.getGamePointVerbal());
        Assert.assertEquals("", p1.getGamePointVerbal());
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals(5, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
        Assert.assertEquals(7, p0.getGamePoint());
        Assert.assertEquals(9, p1.getGamePoint());
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());


        regularRule.apply(players);

        //reset to zero after game is won, for next game

        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(3, p1.getSetScore());

        //below empty value or reseted to zero after game is won, for next game
        Assert.assertEquals("", p0.getSummeryPointVerbal());
        Assert.assertEquals("", p1.getSummeryPointVerbal());
        Assert.assertEquals(0, p0.getGameScore());
        Assert.assertEquals(0, p1.getGameScore());
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals("0", p0.getGamePointVerbal());
        Assert.assertEquals("0", p1.getGamePointVerbal());


    }
}