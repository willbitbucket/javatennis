package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GameActionTest {
    Player p0,p1;
    List<Player> players;
    GameAction gameAction = new GameAction();

    @Test
    public void testWonGame() throws Exception {
        p0 = new Player("A", 4, 6, 1, null);
        p1 = new Player("B", 7, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(7, p1.getGameScore());
    }

    @Test
    public void testNotWinGameAsNotWon2GamesMoreThanOpponent() throws Exception {
        p0 = new Player("A", 6, 6, 1, null);
        p1 = new Player("B", 7, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(6, p0.getGamePoint());
        Assert.assertEquals(7, p1.getGamePoint());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
    }
    @Test
    public void testNotWinGameAsNotWonAtLest7Games() throws Exception {
        p0 = new Player("A", 2, 6, 1, null);
        p1 = new Player("B", 4, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(2, p0.getGamePoint());
        Assert.assertEquals(4, p1.getGamePoint());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(6, p1.getGameScore());
    }
    @Test
    public void testWonGame2() throws Exception {
        p0 = new Player("A", 1, 6, 1, null);
        p1 = new Player("B", 7, 6, 2, p0);
        players = new ArrayList();
        players.add(p0);
        players.add(p1);

        gameAction.execute(players);
        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());
        Assert.assertEquals(6, p0.getGameScore());
        Assert.assertEquals(7, p1.getGameScore());
    }
}