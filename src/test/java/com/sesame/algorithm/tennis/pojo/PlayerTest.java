package com.sesame.algorithm.tennis.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    Player p0,p1;

    @Before
    public void setUp() throws Exception {
        p0 = new Player("A", null);
        p1 = new Player("B", p1);
        p1 = new Player("B", 3, 0, 0, p0);

    }

    @Test
    public void testWonOnePoint() throws Exception {
        p0 = new Player("A", 3, 0, 0, null);
        p0.wonOnePoint();
        Assert.assertEquals(4, p0.getGamePoint());
    }

    @Test
    public void testWonOneGame() throws Exception {
        p0 = new Player("A", 3, 1, 1, null);
        p1 = new Player("B", 3, 0, 2, p0);
        p0.wonOneGame();
        Assert.assertEquals(0, p1.getGameScore());
        Assert.assertEquals(2, p0.getGameScore());

        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(2, p1.getSetScore());

        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());

    }

    @Test
    public void testWonOneSet() throws Exception {
        p0 = new Player("A", 3, 1, 1, null);
        p1 = new Player("B", 3, 0, 2, p0);
        p1.wonOneSet();
        Assert.assertEquals(1, p0.getSetScore());
        Assert.assertEquals(3, p1.getSetScore());

        Assert.assertEquals(0, p0.getGameScore());
        Assert.assertEquals(0, p1.getGameScore());

        Assert.assertEquals(0, p0.getGamePoint());
        Assert.assertEquals(0, p1.getGamePoint());

    }
}