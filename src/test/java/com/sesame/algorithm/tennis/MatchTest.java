package com.sesame.algorithm.tennis;

import com.sesame.test.Util;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class MatchTest {


    @Test
    public void testMatchRegular() throws Exception {
        Match match = new Match("player 1", "player 2");

        match.pointWonBy("player 1");
        match.pointWonBy("player 2");
        // this will return "0-0, 15-15"
        Util.equals("0-0, 15-15", match.score());

        match.pointWonBy("player 1");
        Util.equals("0-0, 30-15", match.score());
        match.pointWonBy("player 1");
        // this will return "0-0, 40-15"
        Util.equals("0-0, 40-15", match.score());

        match.pointWonBy("player 2");
        Util.equals("0-0, 40-30", match.score());
        match.pointWonBy("player 2");
        // this will return "0-0, Deuce"
        Util.equals("0-0, Dence", match.score());

        match.pointWonBy("player 1");
        // this will return "0-0, Advantage player 1"
        Util.equals("0-0, Advantage player 1", match.score());

        match.pointWonBy("player 1");
        // this will return "1-0"
        Util.equals("1-0",match.score());
        match.pointWonBy("player 1");
        Util.equals("1-0, 15-0",match.score());
        match.pointWonBy("player 2");
        Util.equals("1-0, 15-15", match.score());
        match.pointWonBy("player 2");
        Util.equals("1-0, 15-30", match.score());
        match.pointWonBy("player 1");
        Util.equals("1-0, 30-30", match.score());
        match.pointWonBy("player 2");
        Util.equals("1-0, 30-40",match.score());
        match.pointWonBy("player 1");
        Util.equals("1-0, Dence",match.score());
        match.pointWonBy("player 2");
        Util.equals("1-0, Advantage player 2",match.score());

    }

    @Test
    public void testMatchBreak() throws Exception {
        Match match = new Match("player 1", "player 2");

        match.pointWonBy("player 1");
        match.pointWonBy("player 2");
        // this will return "0-0, 15-15"
        match.score();

        match.pointWonBy("player 1");
        match.pointWonBy("player 1");
        // this will return "0-0, 40-15"
        match.score();

        match.pointWonBy("player 2");
        match.pointWonBy("player 2");
        // this will return "0-0, Deuce"
        match.score();

        match.pointWonBy("player 1");
        // this will return "0-0, Advantage player 1"
        match.score();

        match.pointWonBy("player 1");
        // this will return "1-0"
        match.score();
    }


    @Test
    public void testRandomMatch(){

        Match match = new Match("player 1", "player 2");

        for(int i=0;i<200;i++) {

            long result = Math.round( Math.random() ) ;
            match.pointWonBy("player "+String.valueOf(result+1));
            match.score();
    }


    }
}