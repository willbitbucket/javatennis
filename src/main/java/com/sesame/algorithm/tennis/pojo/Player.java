package com.sesame.algorithm.tennis.pojo;

/**
 * Created by Will on 2017/5/14.
 */
public class Player {
    private Player opponent;
    private String name;

    private int gamePoint;//current points in the game
    private int gameScore;//current overall game scores
    private int setScore;//current set scores
    private String gamePointVerbal;//current verbal statement for the gamePoint
    private String summeryPointVerbal;//current summery verbal statement for the gamePoint, e.g. Advantage xyz


    public void wonOnePoint(){
        this.gamePoint++;
    }

    /**
     * once won one game, increment game score,and reset game point for next round
     * also reset for opponent
     */
    public void wonOneGame(){
        this.gameScore++;
        this.resetGamePoint();
        this.getOpponent().resetGamePoint();
    }

    /**
     * increment set score, reset game score
     */
    public void wonOneSet(){
        this.setScore++;
        this.resetGameScore();
        this.getOpponent().resetGameScore();
        this.resetGameScore();
        this.getOpponent().resetGameScore();
    }

    /**
     *
      * @param name Name of the player
     * @param opponent Can be null when the first player is inited, but when initializing the second player, must assign the first player to play against
     */
    public Player(String name, Player opponent) {
        this.name = name;
        this.gamePoint = 0;
        this.gameScore = 0;
        this.setScore = 0;
        this.gamePointVerbal = "";
        this.summeryPointVerbal="";
        this.initOpponent(opponent);
    }

    /**
     *
     * @param name
     * @param gamePoint Current points in the game
     * @param gameScore Current overall game scores
     * @param setScore Current set scores
     * @param opponent
     */
    public Player(String name, int gamePoint, int gameScore, int setScore,  Player opponent) {
        this.name = name;
        this.gamePoint = gamePoint;
        this.gameScore = gameScore;
        this.setScore = setScore;
        this.gamePointVerbal = "";
        this.summeryPointVerbal="";
        this.initOpponent(opponent);
    }

    protected void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    private void initOpponent(Player opponent){
        if(opponent!=null) {
            this.opponent = opponent;
            opponent.setOpponent(this);
        }
    }

    private void resetGamePoint(){
        this.gamePoint = 0;
    }

    /**
     * reset game score will automatically reset game point too
     */
    private void resetGameScore(){
        this.gameScore = 0;
        this.resetGamePoint();
    }

    public Player getOpponent() {
        return opponent;
    }

    public int getGameScore() {
        return gameScore;
    }

    public int getGamePoint() {
        return gamePoint;
    }

    public String getSummeryPointVerbal() {
        return summeryPointVerbal;
    }

    public void setSummeryPointVerbal(String summeryPointVerbal) {
        this.summeryPointVerbal = summeryPointVerbal;
    }

    public String getGamePointVerbal() {
        return gamePointVerbal;
    }

    public void setGamePointVerbal(String gamePointVerbal) {
        this.gamePointVerbal = gamePointVerbal;
    }

    public int getSetScore() {
        return setScore;
    }

    public String getName() {
        return name;
    }

}
