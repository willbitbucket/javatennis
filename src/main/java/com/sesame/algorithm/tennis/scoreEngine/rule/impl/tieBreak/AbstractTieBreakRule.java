package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public abstract class AbstractTieBreakRule implements Rule, Action{
    @Override
    public void apply(List<Player> players) {

        Player player = players.get(0);
        if(player.getGameScore()==6 &&
                player.getOpponent().getGameScore()==6){
            execute(players);
        }
    }

}
