package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */


//* If at least 3 points have been scored by each player, and the scores are equal, the score is "deuce".
//* If at least 3 points have been scored by each side and a player has one more point than his opponent, the score of the game is "advantage" for the player in the lead.
public class PointVerbalSummeryAction implements Action {

    @Override
    public void execute(List<Player> players) {
        Player p0 = players.get(0);
        Player p1 = players.get(1);

        if(!(p0.getGamePoint()>=3
             &&
             p1.getGamePoint()>=3)
                )
        {// make GameVerbal = 15 - 15, etc
        if (!(p0.getGamePoint() == 0 && p1.getGamePoint() == 0))
                {
                    p0.setSummeryPointVerbal(p0.getGamePointVerbal() + "-" + p1.getGamePointVerbal());
                    p1.setSummeryPointVerbal(p0.getGamePointVerbal() + "-" + p1.getGamePointVerbal());
                }
                else//resetted to zero, i.e. will start a new game, summeryPointVerbal becomes empty
                {
                    p0.setSummeryPointVerbal("");
                    p1.setSummeryPointVerbal("");
                }
        }
        else
        {
            if(p0.getGamePoint() == p1.getGamePoint())
            {
                p0.setSummeryPointVerbal("Dence");
                p1.setSummeryPointVerbal("Dence");
            }
            else
            {
                String s;
                if(p0.getGamePoint()>p0.getOpponent().getGamePoint())
                {
                    s = "Advantage " + p0.getName();
                }
                else
                {
                    s = "Advantage " + p1.getName();
                }
                p0.setSummeryPointVerbal(s);
                p1.setSummeryPointVerbal(s);
            }
        }

    }
}
