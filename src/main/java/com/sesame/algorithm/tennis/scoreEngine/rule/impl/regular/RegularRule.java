package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public class RegularRule extends AbstractRegularRule {
    private ArrayList<Action> actions;
    public RegularRule(){
        actions = new ArrayList();

        //sequence does matter, acting from game, and then set and then match, before updating the point verbal and summery verbal
        actions.add(new GameAction());
        actions.add(new SetAction());
        actions.add(new MatchAction());
        actions.add(new PointVerbalAction());
        actions.add(new PointVerbalSummeryAction());
    }

    @Override
    public void execute(List<Player> players) {
        for(Action action:actions){
            action.execute(players);
        }

    }

}
