package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.AbstractTieBreakRule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */


// Instead of being scored from 0, 15, 30, 40 like regular games,
// the score for a tie breaker goes up incrementally from 0 by 1.
// i.e a player's score will go from 0 to 1 to 2 to 3 …etc.
public class PointVerbalAction extends AbstractTieBreakRule {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
            p.setGamePointVerbal(String.valueOf(p.getGamePoint()));
        }
    }
}
