package com.sesame.algorithm.tennis.scoreEngine.impl;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.Engine;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public class SimpleEngine implements Engine {
    private List<Rule> rules = new ArrayList();

    public SimpleEngine appendRule(Rule r){
        rules.add(r);
        return this;
    }

    public void apply(List<Player> players){
        for(Rule rule:rules){
            rule.apply(players);
        }
    }

    @Override
    public List<Rule> getRules() {
        return rules;
    }
}
