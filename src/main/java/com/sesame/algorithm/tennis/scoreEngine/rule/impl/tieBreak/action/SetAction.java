package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.AbstractRegularRule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */

// Tie-break, A player wins a set by winning at least 7 games and the opponent with 6 games.
public class SetAction extends AbstractRegularRule {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
            if(p.getGameScore()>=7
                    &&
               p.getOpponent().getGameScore()==6)
                p.wonOneSet();
        }
    }
}
