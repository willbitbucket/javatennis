package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.AbstractTieBreakRule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */


// A tie-break is scored one point at a time.
// The tie-break game continues until
// one player wins seven points by a margin of two or more points.

public class GameAction extends AbstractTieBreakRule {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
            if(p.getGamePoint()>=7
                    &&
               p.getGamePoint()>=p.getOpponent().getGamePoint()+2)
                p.wonOneGame();
        }
    }
}
