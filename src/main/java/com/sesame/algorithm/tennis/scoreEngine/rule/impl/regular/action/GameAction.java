package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */

// A game is won by the first player to have won at least 4 points in total and at least 2 points more than the opponent.
public class GameAction implements Action {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
            if(p.getGamePoint()>=4
                    &&
               p.getGamePoint()>=p.getOpponent().getGamePoint()+2)
                p.wonOneGame();
        }
    }
}
