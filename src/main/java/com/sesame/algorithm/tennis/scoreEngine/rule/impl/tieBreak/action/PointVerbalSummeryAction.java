package com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.AbstractRegularRule;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.AbstractTieBreakRule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */


// a player's score will go from 0 to 1 to 2 to 3 …etc.//* If at least 3 points have been scored by each side and a player has one more point than his opponent, the score of the game is "advantage" for the player in the lead.
public class PointVerbalSummeryAction extends AbstractTieBreakRule {

    @Override
    public void execute(List<Player> players) {
        Player p0 = players.get(0);
        Player p1 = players.get(1);

        if (!(p0.getGamePoint() == 0 && p1.getGamePoint() == 0))
        {
            p0.setSummeryPointVerbal(p0.getGamePointVerbal() + "-" + p1.getGamePointVerbal());
            p1.setSummeryPointVerbal(p0.getGamePointVerbal() + "-" + p1.getGamePointVerbal());
        }
        else
        {
            p0.setSummeryPointVerbal("");
            p1.setSummeryPointVerbal("");
        }
    }
}
