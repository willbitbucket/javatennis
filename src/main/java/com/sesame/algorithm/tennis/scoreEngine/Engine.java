package com.sesame.algorithm.tennis.scoreEngine;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public interface  Engine {

    public Engine appendRule(Rule r);

    public void apply(List<Player> players);

    public List<Rule> getRules();
}
