package com.sesame.algorithm.tennis.scoreEngine.rule;

import com.sesame.algorithm.tennis.pojo.Player;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public interface Rule {
    public void apply(List<Player> players);
}
