package com.sesame.algorithm.tennis.scoreEngine.factory;

import com.sesame.algorithm.tennis.scoreEngine.Engine;
import com.sesame.algorithm.tennis.scoreEngine.impl.SimpleEngine;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public class SimpleEngineFactory {

    public static Engine newEngine(List<Rule> rules){
        Engine engine = new SimpleEngine();
        for(Rule r: rules)
        {
            engine.appendRule(r);
        }
        return engine;
    }
}
