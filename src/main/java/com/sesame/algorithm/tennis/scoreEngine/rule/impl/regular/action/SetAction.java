package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */

// A player wins a set by winning at least 6 games and at least 2 games more than the opponent.
public class SetAction implements Action {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
            if(p.getGameScore()>=6
                    &&
               p.getGameScore()>=p.getOpponent().getGameScore()+2)
                p.wonOneSet();
        }
    }
}
