package com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.action;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.rule.Action;

import java.util.List;

/**
 * Created by Will on 2017/5/14.
 */
public class PointVerbalAction implements Action {

    @Override
    public void execute(List<Player> players) {
        for(Player p:players){
           switch (p.getGamePoint()) {
               case 0:
                   p.setGamePointVerbal("0");
                   break;
               case 1:
                   p.setGamePointVerbal("15");
                   break;
               case 2:
                   p.setGamePointVerbal("30");
                   break;
               case 3:
                   p.setGamePointVerbal("40");
                   break;
               default:
                   p.setGamePointVerbal("");
                   break;
           }
        }



    }
}
