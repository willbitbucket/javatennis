package com.sesame.algorithm.tennis;

import com.sesame.algorithm.tennis.pojo.Player;
import com.sesame.algorithm.tennis.scoreEngine.Engine;
import com.sesame.algorithm.tennis.scoreEngine.factory.SimpleEngineFactory;
import com.sesame.algorithm.tennis.scoreEngine.rule.Rule;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.regular.*;
import com.sesame.algorithm.tennis.scoreEngine.rule.impl.tieBreak.TieBreakRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Will on 2017/5/14.
 */
public class Match {
    private List<Player> players;
    private Map<String, Player> name2player;
    private Engine engine;

    public Match(String p0, String p1){

        Player player0 = new Player(p0, null);
        Player player1 = new Player(p1, player0);

        players = new ArrayList<Player>(2);
        players.add(player0);
        players.add(player1);

        name2player = new HashMap<String, Player>();
        name2player.put(player0.getName(), player0);
        name2player.put(player1.getName(), player1);

        ArrayList<Rule> rules = new ArrayList();

        //1.regular rules
        rules.add(new RegularRule());

        //2. tie break rules
        rules.add(new TieBreakRule());

        engine = SimpleEngineFactory.newEngine(rules);

    }

    public void pointWonBy(String pName){
        Player player = name2player.get(pName);
        if(player == null) throw new UnsupportedOperationException("Player not found");
        player.wonOnePoint();
        engine.apply(players);
    }

    public String score(){
        Player p0 = players.get(0);
        Player p1 = players.get(1);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(p0.getGameScore()).append("-").append(p1.getGameScore());
        if(!p0.getSummeryPointVerbal().isEmpty())
            stringBuffer.append(", ").append(p0.getSummeryPointVerbal());
        System.out.println(stringBuffer.toString());
        return stringBuffer.toString();
    }
}
